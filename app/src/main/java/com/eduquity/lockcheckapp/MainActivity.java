package com.eduquity.lockcheckapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.DataOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "KioskExample";
    private DevicePolicyManager dpm;
    private boolean inKioskMode;
    private ComponentName deviceAdmin;
    private Process process = null;
    private DataOutputStream os = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                deviceAdmin = new ComponentName(getBaseContext(), AdminReceiver.class);

                /*if(!dpm.isAdminActive(deviceAdmin))
                {
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdmin);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "");
                    startActivityForResult(intent, 0);
                }*/
                lockScreen();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dislockScreen();
            }
        });

        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
            }
        });

        execShellEx("com.eduquity.lockcheckapp/com.eduquity.lockcheckapp.AdminReceiver");

    }

    private void execShellEx(String cmd){
        String cmdString ="dpm set-device-owner "+cmd;
        try {
            Runtime.getRuntime().exec(new String[] {"su", "-c", cmdString});
        } catch (IOException e) {
            Log.d(TAG," eeeee:"+e);
            e.printStackTrace();
        }
    }

    private boolean doLockScreen() {
        if (dpm.isLockTaskPermitted(this.getPackageName())) {
            Log.i(TAG, "start lock screen");
            startLockTask();
            inKioskMode = true;
            Log.i(TAG, "lock screen success");
            return true;
        }
        Log.w(TAG, "cannot lock screen");
        return false;
    }

    private void lockScreen() {
        try {
            if (!inKioskMode) {
                dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                deviceAdmin = new ComponentName(this, AdminReceiver.class);
                Log.e(TAG, "isAdminActive: " + dpm.isAdminActive(deviceAdmin) + "\tisDeviceOwnerApp: " + dpm.isDeviceOwnerApp(getPackageName()));
                if (dpm.isDeviceOwnerApp(getPackageName())) {
                    //如果这里失效，请使用adb shell命令设置deviceOwnerAPP为当前app $ adb shell dpm set-device-owner com.honghe.screenlocktest/.AdminReceiver
                    //参考 https://juejin.im/entry/578f873dd342d30058e99c51
                    dpm.setLockTaskPackages(deviceAdmin, new String[]{getPackageName()});
                    Log.e(TAG, "setLockTaskPackages: "+getPackageName());
                }
                doLockScreen();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
        }
    }

    private void dislockScreen() {

        try {
            if (inKioskMode) {
                stopLockTask();
                inKioskMode = false;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
        }
    }
}